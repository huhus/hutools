package huprotocol

import (
	"log"

	"gitee.com/huhus/hutools/humodbus"
)

// map key name
const (
	ValLizi03 = "MP03"
	ValLizi05 = "MP05"
	ValLizi20 = "MP20"
	ValLizi50 = "MP50"
	ValWendu  = "TEMP"
	ValShidu  = "HUMI"
)

var (
	RJ45RTU_cmd_body = []byte{0x03, 0x00, 0x00, 0x00, 0x02}
	RJ45TCP_cmd_body = []byte{0x03, 0x00, 0x00, 0x00, 0x0a}
	// tcp_head = []byte{0x01, 0x00, 0x00, 0x00, 0x00, 0x06}
)

type ZKZQRTUProt struct {
}

type ZKZQTCPProt struct {
}

// 读数据
func (ZKZQRTUProt) ReadCMD(slaveid byte) (cmd []byte) {
	cmd = append(cmd, slaveid)
	cmd = append(cmd, RJ45RTU_cmd_body...)
	crc := humodbus.CRC16Byte(cmd)
	cmd = append(cmd, crc[1])
	cmd = append(cmd, crc[0])
	return cmd
}

// 解析读取数据响应包
func (ZKZQRTUProt) DecodeReadResp(resp []byte, args ...byte) (datamp map[string]interface{}) {
	slaveid := byte(0x01)
	if len(args) > 0 {
		slaveid = args[0]
	}
	if len(resp) != 9 {
		log.Println("resp len error")
		return nil
	} else {
		if resp[0] != slaveid {
			log.Println("resp slaveid error")
			return nil
		} else {
			if !humodbus.ModbusCRCCheck(resp) {
				log.Println("crc check error")
				return nil
			} else {
				datamp = make(map[string]interface{})
				// 温度补偿0，湿度补偿5
				datamp[ValWendu] = float64(humodbus.Bytes2Data(resp[3:5], humodbus.DataTypeInt).(int)) / 10.0
				datamp[ValShidu] = float64(humodbus.Bytes2Data(resp[5:7], humodbus.DataTypeInt).(int)) / 10.0
			}
		}
	}
	return
}

// 不是modbustcp 用得透传
func (ZKZQTCPProt) ReadCMD(slaveid byte, arg ...byte) (cmd []byte) {
	cmd = append(cmd, slaveid)
	cmd = append(cmd, RJ45TCP_cmd_body...)
	// cmd = append(humodbus.AddMBAPHead(cmd, arg...), cmd...)
	crc := humodbus.CRC16Byte(cmd)
	cmd = append(cmd, crc[1])
	cmd = append(cmd, crc[0])
	return cmd
}

func (ZKZQTCPProt) DecodeReadResp(resp []byte, args ...byte) (datamp map[string]interface{}) {
	if len(resp) != 25 {
		log.Println("resp len error", len(resp))
		return nil
	} else {
		for i := 0; i < len(args); i++ {
			if resp[i] != args[i] {
				log.Println("TX id error")
				return nil
			}
		}
		datamp = make(map[string]interface{})
		datamp[ValLizi03] = humodbus.Bytes2Data(resp[3:7], humodbus.DataTypeLong)
		datamp[ValLizi05] = humodbus.Bytes2Data(resp[7:11], humodbus.DataTypeLong)
		datamp[ValLizi20] = humodbus.Bytes2Data(resp[15:19], humodbus.DataTypeLong)
		datamp[ValLizi50] = humodbus.Bytes2Data(resp[19:23], humodbus.DataTypeLong)
	}
	return
}

// 适配中盛易达无尘度
func (ZKZQTCPProt) DecodeReadRespZSYD(resp []byte, args ...byte) (datamp map[string]interface{}) {
	if len(resp) != 25 {
		log.Println("resp len error", len(resp))
		return nil
	} else {
		for i := 0; i < len(args); i++ {
			if resp[i] != args[i] {
				log.Println("TX id error")
				return nil
			}
		}
		datamp = make(map[string]interface{})
		datamp[ValLizi03] = humodbus.Bytes2Data(humodbus.GetDCBA(resp[3:7], humodbus.BADC), humodbus.DataTypeLong)
		datamp[ValLizi05] = humodbus.Bytes2Data(humodbus.GetDCBA(resp[7:11], humodbus.BADC), humodbus.DataTypeLong)
		datamp[ValLizi20] = humodbus.Bytes2Data(humodbus.GetDCBA(resp[15:19], humodbus.BADC), humodbus.DataTypeLong)
		datamp[ValLizi50] = humodbus.Bytes2Data(humodbus.GetDCBA(resp[19:23], humodbus.BADC), humodbus.DataTypeLong)
	}
	return
}
