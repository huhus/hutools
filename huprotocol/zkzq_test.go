package huprotocol

import (
	"log"
	"testing"
)

func Test(t *testing.T) {
	zkp := ZKZQRTUProt{}
	log.Printf("%02x \n", zkp.ReadCMD(2))
	// 微尘粒子 = 0x0115  个= 277 个
	// 温度 = 0x0103 /10 ℃ = 25.9 ℃
	// 湿度 = 0x0203 /10 %RH = 51.5 %RH
	resp := []byte{0x01, 0x03, 0x06, 0x01, 0x15, 0x01, 0x03, 0x02, 0x03, 0x9d, 0xfa}
	log.Println(zkp.DecodeReadResp(resp, 1))
}

func TestTCP(t *testing.T) {
	zktcpp := ZKZQTCPProt{}
	log.Printf("%02x \n", zktcpp.ReadCMD(1, 1, 1))
	resp := []byte{0x01, 0x01, 0x00, 0x00, 0x00, 0x09, 0x01, 0x03, 0x06, 0x01, 0x15, 0x00, 0x00, 0x00, 0x00}
	log.Println(zktcpp.DecodeReadResp(resp, 1, 1))
}
