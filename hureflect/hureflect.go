package hureflect

import (
	"log"
	"reflect"
)

// GetVal 获取结构体对象o的fieldname属性的值
func GetVal(o interface{}, fieldname string) (sv reflect.Value) {
	sv = reflect.Value{}
	t := reflect.TypeOf(o)
	ve := reflect.ValueOf(o)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
		ve = ve.Elem()
	}
	if t.Kind() != reflect.Struct {
		log.Println("Check type error not Struct")
	}
	fieldNum := t.NumField()

	for ni := 0; ni < fieldNum; ni++ {
		// if strings.ToUpper(t.Field(i).Name) == strings.ToUpper(columnName) {
		name := t.Field(ni).Name
		if name == fieldname {
			sv = ve.FieldByName(t.Field(ni).Name)
		}
	}
	return
}

// SetVal 给结构体指针对象o的fieldname属性赋值为 value。
func SetVal(o interface{}, fieldname string, value reflect.Value) (ok bool) {
	t := reflect.TypeOf(o)
	ve := reflect.ValueOf(o)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
		ve = ve.Elem()
	} else {
		log.Println("Is not Ptr")
		return
	}
	if t.Kind() != reflect.Struct {
		log.Println("Check type error not Struct")
		return
	}
	fieldNum := t.NumField()

	for ni := 0; ni < fieldNum; ni++ {
		// if strings.ToUpper(t.Field(i).Name) == strings.ToUpper(columnName) {
		name := t.Field(ni).Name
		if name == fieldname {
			sv := ve.FieldByName(t.Field(ni).Name)
			// 类型一致才赋值
			if value.Type() == sv.Type() {
				sv.Set(value)
				ok = true
			} else {
				log.Println("Type no equal")
			}
		}
	}
	return
}
