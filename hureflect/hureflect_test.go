package hureflect

import (
	"log"
	"reflect"
	"testing"
)

func Test_get_demo(t *testing.T) {
	type A struct {
		Name string
		Age  int
	}
	a := A{"hhh", 0}
	name := "Age"
	val := GetVal(&a, name)
	log.Println(name, val)
}

func Test_set_demo(t *testing.T) {
	type B struct {
		Val int
	}
	type A struct {
		Name string
		Age  int
		Bv   B
	}
	a := A{"hhh", 0, B{}}
	name := "Bv"
	log.Println(SetVal(&a, name, reflect.ValueOf(B{123})))
	// val := GetVal(&a, name)
	// log.Println(name, val)
	log.Println(a)
}
