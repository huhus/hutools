package humq

import (
	"fmt"
	"log"
	"strconv"
	"testing"
	"time"
)

func Test(t *testing.T) {
	//Simple模式 发送者
	rabbitmq := NewRabbitMQSimple("imoocSimple")
	defer rabbitmq.Destory()
	for {
		fmt.Println(time.Now().Format("2006-01-02 15:04:05"))
		rabbitmq.PublishSimple(time.Now().Format("2006-01-02 15:04:05"))
		time.Sleep(time.Second)
	}
}

func Test2(t *testing.T) {
	//接收者
	rabbitmq := NewRabbitMQSimple("imoocSimple")
	defer rabbitmq.Destory()
	rabbitmq.ConsumeSimple()

	// rabbitmq2 := NewRabbitMQSimple("imoocSimple")
	// defer rabbitmq2.Destory()
	// rabbitmq2.ConsumeSimple()

}

func TestPubSub(t *testing.T) {
	//订阅模式发送者
	rabbitmq := NewRabbitMQPubSub("" + "newProduct")
	defer rabbitmq.Destory()
	for i := 0; i < 30; i++ {
		rabbitmq.PublishPub("订阅模式生产第" + strconv.Itoa(i) + "条数据")
		log.Println(i)
		time.Sleep(1 * time.Second)
	}
}

func TestPubSubC(t *testing.T) {
	//接收者
	rabbitmq2 := NewRabbitMQPubSub("" + "newProduct")
	defer rabbitmq2.Destory()
	go rabbitmq2.RecieveSub()

	rabbitmq3 := NewRabbitMQPubSub("" + "newProduct")
	defer rabbitmq3.Destory()
	rabbitmq3.RecieveSub()
}

func TestTopic(t *testing.T) {
	//Topic模式发送者
	imoocOne := NewRabbitMQTopic("exImoocTopic", "three.imooc.topic88")
	imoocTwo := NewRabbitMQTopic("exImoocTopic", "four.imooc.topic88")

	for i := 0; i <= 10; i++ {
		imoocOne.PublishTopic("hello imooc topic three!" + strconv.Itoa(i))
		imoocTwo.PublishTopic("hello imooc topic four!" + strconv.Itoa(i))
		time.Sleep(1 * time.Second)
		log.Println(i)
	}
}

func TestTopicC(t *testing.T) {
	//接收者
	// rabbitmq2 := NewRabbitMQTopic("exImoocTopic", "three.imooc.topic88")
	rabbitmq2 := NewRabbitMQTopic("exImoocTopic", "three.#")
	defer rabbitmq2.Destory()
	rabbitmq2.RecieveTopic()
}

func TestRouter(t *testing.T) {
	//Topic模式发送者
	imoocOne := NewRabbitMQRouting("exImooc", "imooc_one")
	imoocTwo := NewRabbitMQRouting("exImooc", "imooc_two")

	for i := 0; i <= 10; i++ {
		imoocOne.PublishRouting("hello imooc one!" + strconv.Itoa(i))
		imoocTwo.PublishRouting("hello imooc two!" + strconv.Itoa(i))
		time.Sleep(1 * time.Second)
		log.Println(i)
	}
}

func TestRouterC(t *testing.T) {
	//接收者
	rabbitmq2 := NewRabbitMQRouting("exImooc", "imooc_two")
	defer rabbitmq2.Destory()
	rabbitmq2.RecieveRouting()
}
