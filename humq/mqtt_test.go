package humq

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sync"
	"testing"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

const broker = "tcp://106.52.150.139:1883"

// const broker = "tcp://127.0.0.1:1883"
const username = "mqtt-test"
const password = "mqtt-test"
const ClientID = "AO8"
const ClientID2 = "AO82"

//message的回调
var onMessage mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("[%s] -> %s\n", msg.Topic(), msg.Payload())
}

var wg sync.WaitGroup
var client mqtt.Client
var client2 mqtt.Client

func TestFile(t *testing.T) {
	//打开并新建文件
	file, err := os.Open("\\\\192.168.1.108\\gg\\23.txt")
	if err != nil {
		fmt.Println("err = ", err)
		return
	}

	//使用完毕，关闭文件
	defer file.Close()
	r := bufio.NewReader(file)
	//重复循环
	for {
		//遇到'\n'结束读取，但是'\n'也能读进去
		buf, err := r.ReadBytes('\n') //此处要用单引号
		if err != nil {
			if err == io.EOF {
				time.Sleep(time.Second * 1)
				//文件已经结束，结束循环，结束读取
				// break
			}
			// fmt.Println("err = ", err)
		}
		fmt.Printf("buf = ##%s##\n", string(buf))
	}

}

func TestMain(t *testing.T) {
	//连接MQTT服务器
	client = mqttConnect(ClientID)
	client2 = mqttConnect(ClientID2)
	defer client.Disconnect(250)  //注册销毁
	defer client2.Disconnect(250) //注册销毁
	wg.Add(1)
	go mqttSubScribe("topic/test")
	wg.Add(1)
	go testPublish()
	// wg.Add(1)
	// go testPublish2()
	wg.Wait()
}
func mqttConnect(ClientID string) mqtt.Client {
	//配置
	clinetOptions := mqtt.NewClientOptions().AddBroker(broker).SetUsername(username).SetPassword(password)
	clinetOptions.SetClientID(ClientID)
	clinetOptions.SetConnectTimeout(time.Duration(60) * time.Second)
	//连接
	c := mqtt.NewClient(clinetOptions)
	//客户端连接判断
	if token := c.Connect(); token.WaitTimeout(time.Duration(60)*time.Second) && token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	return c
}

func mqttSubScribe(topic string) {

	defer wg.Done()
	for {
		token := client.Subscribe(topic, 1, onMessage)
		token.Wait()
	}
}

//测试 3秒发送一次，然后自己接收
func testPublish() {
	defer wg.Done()
	for {
		client2.Publish("topic/test", 1, false, "TEST"+time.Now().Format("2006-01-02 15:04:05"))
		time.Sleep(time.Duration(3) * time.Second)
	}
}

func testPublish2() {
	defer wg.Done()
	for {
		client.Publish("topic/test", 1, false, "AAAA"+time.Now().Format("2006-01-02 15:04:05"))
		time.Sleep(time.Duration(3) * time.Second)
	}
}
