package humap

import (
	"github.com/gogf/gf/container/gmap"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/util/gconv"
)

// ***************************
type MS []map[string]interface{}

type MU interface {
	Sum(keynames ...string) interface{}
	Merge(keyname string, ms2 interface{}) interface{}
	Merges(keyname string, mss ...interface{}) interface{}
}

func (ms MS) Sum(keynames ...string) interface{} {
	return MapSum(ms, keynames...)
}

func (ms MS) Merge(keyname string, ms2 interface{}) interface{} {
	return MergeMapSlice(keyname, ms, ms2.(MS))
}

func (ms MS) Merges(keyname string, mss ...interface{}) interface{} {
	agrs := []MS{}
	agrs = append(agrs, ms)
	for _, v := range mss {
		agrs = append(agrs, v.(MS))
	}
	return MergeMapSlices(keyname, agrs...)
}

func Sum(i MU, ks ...string) interface{} {
	return i.Sum(ks...)
}

func Merge(i MU, keyname string, ms2 interface{}) interface{} {
	return i.Merge(keyname, ms2)
}

func Merges(i MU, keyname string, ms2 ...interface{}) interface{} {
	return i.Merges(keyname, ms2...)
}

// ***************************

// 根据 keynames 求和
func MapSum(ms []map[string]interface{}, keynames ...string) map[string]interface{} {
	m := map[string]interface{}{}
	if len(keynames) > 0 {
		for _, m1 := range ms {
			for _, keyname := range keynames {
				m[keyname] = gconv.Float32(m1[keyname]) + gconv.Float32(m[keyname])
			}
		}
	}
	m["name"] = "sum"
	return m
}

// 根据 keynames 求和
func ResultSum(ms gdb.Result, keynames ...string) map[string]interface{} {
	m := map[string]interface{}{}
	if len(keynames) > 0 {
		for _, m1 := range ms {
			for _, keyname := range keynames {
				m[keyname] = gconv.Float32(m1[keyname]) + gconv.Float32(m[keyname])
			}
		}
	}
	m["name"] = "sum"
	return m
}

// 根据 keyname 取最后1条结果
func ResultLast(ms gdb.Result, keyname string) map[string]interface{} {
	res := map[string]interface{}{}
	for _, m := range ms {
		res[m[keyname].String()] = m
	}
	return res
}

// 根据 keynames[0] 取第一条结果
func ResultOne(ms gdb.Result, keyname string) map[string]interface{} {
	res := map[string]interface{}{}
	for _, m := range ms {
		if _, ok := res[m[keyname].String()]; !ok {
			res[m[keyname].String()] = m
		}
	}
	return res
}

// 根据keyname 把 ms2 合并到 ms1
// func MergeMapSlice(keyname string, ms1, ms2 []map[string]interface{}) []map[string]interface{} {
func MergeMapSlice(keyname string, ms1, ms2 []map[string]interface{}) MS {
	for _, m1 := range ms1 {
		for _, m2 := range ms2 {
			if m2[keyname] == m1[keyname] {
				for k2, v2 := range m2 {
					m1[k2] = v2
				}
			}
		}
	}
	return ms1
}

// 根据keyname 把 mss[1]... 合并到 mss[0]
func MergeMapSlices(keyname string, mss ...MS) []map[string]interface{} {
	size := len(mss)
	for i := 1; i < size; i++ {
		mss[0] = MergeMapSlice(keyname, mss[0], mss[i])
	}
	return mss[0]
}

// func MergeMapSlicesi(keyname string, mss ...interface{}) []map[string]interface{} {
// 	size := len(mss)
// 	for i := 1; i < size; i++ {
// 		mss[0] = MergeMapSlice(keyname, mss[0].(MS), mss[i].(MS))
// 		fmt.Println(mss[0])
// 	}
// 	return mss[0].(MS)
// }

// 合并gdb.Results
func MergeResult(keyname string, ms1, ms2 gdb.Result) gdb.Result {
	for _, m1 := range ms1 {
		for _, m2 := range ms2 {
			if m2[keyname].Val() == m1[keyname].Val() {
				for k2, v2 := range m2 {
					m1[k2] = v2
				}
			}
		}
	}
	return ms1
}

// 合并多个gdb.Result
func MergeResults(keyname string, mss ...gdb.Result) gdb.Result {
	size := len(mss)
	for i := 1; i < size; i++ {
		mss[0] = MergeResult(keyname, mss[0], mss[i])
	}
	return mss[0]
}

// 合并map
func MergeGmap(keyname string, ms1, ms2 []*gmap.StrAnyMap) []*gmap.StrAnyMap {
	for _, m1 := range ms1 {
		for _, m2 := range ms2 {
			if m2.Get(keyname) == m1.Get(keyname) {
				m1.Sets(m2.Map())
			}
		}
	}
	return ms1
}

// 合并maps
func MergeGmaps(keyname string, mss ...[]*gmap.StrAnyMap) []*gmap.StrAnyMap {
	size := len(mss)
	for i := 1; i < size; i++ {
		mss[0] = MergeGmap(keyname, mss[0], mss[i])
	}
	return mss[0]
}
