package huerror

import (
	"log"
	"os"
	"runtime/debug"
	"time"
)

func ErrorHandler(err error, why ...string) bool {
	if err != nil {
		log.Println(why, err)
		return true
	}
	return false
}

func Recover() {
	if err := recover(); err != nil {
		log.Println(err)
	}
}

func TryE() {
	errs := recover()
	if errs == nil {
		return
	}
	exeName := os.Args[0] //获取程序名称

	now := time.Now()  //获取当前时间
	pid := os.Getpid() //获取进程ID

	time_str := now.Format("20060102150405") //设定时间格式
	log.Println(exeName, pid, time_str)      // 程序名 进程ID 当前时间（年月日时分秒）

	log.Println(string(debug.Stack())) //输出堆栈信息
}
