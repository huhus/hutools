package hutime

import (
	"time"

	"gitee.com/huhus/hutools/hustrings"
)

const (
	Ymdhms   string = "2006-01-02 15:04:05"
	Ymd      string = "2006-01-02"
	YmdClean string = "20060102"
)

// Parse layout
// 	Ymdhms = "2006-01-02 15:04:05"
// 	Ymd    = "2006-01-02"
func Parse(layout string, timeStr string) (time.Time, error) {
	return time.Parse(layout, timeStr)
}

func Date(t time.Time) string {
	return t.Format("2006-01-02")
}

func Now() *time.Time {
	n := time.Now()
	return &n
}

func Yestoday(t ...time.Time) time.Time {
	if len(t) == 1 {
		return t[0].AddDate(0, 0, -1)
	}
	return time.Now().AddDate(0, 0, -1)
}

func QianTian(args ...string) string {
	if len(args) == 2 {
		t, _ := Parse(args[0], args[1])
		return Date(AddDay(-2, t))
	}
	return Date(AddDay(-2))
}

func AddDay(number int, t ...time.Time) time.Time {
	if len(t) == 1 {
		return t[0].AddDate(0, 0, number)
	}
	return time.Now().AddDate(0, 0, number)
}

func Monday() time.Time {
	offset := int(time.Monday - time.Now().Weekday())
	// Sunday
	if offset > 0 {
		offset = -6
	} else if offset == 0 { // Monday
		offset = -7
	}
	return time.Now().AddDate(0, 0, offset)
}

func Sunday() time.Time {
	offset := int(time.Sunday - time.Now().Weekday())
	// Sunday
	if offset == 0 { // Monday
		offset = -7
	}
	return time.Now().AddDate(0, 0, offset)
}

// MonthFirstDay 2021年9月23日
// huhutime.MonthFirstDay() ==> 2021-09-01
// huhutime.MonthFirstDay("02") ==> 2021-02-01
// huhutime.MonthFirstDay("1999", "02") ==> 1999-02-01
func MonthFirstDay(args ...string) string {
	if len(args) == 1 {
		// time.Now().Format("2006") + "-" + args[0] + "-01"
		s, _ := hustrings.BuilderWriteStr(time.Now().Format("2006"), "-", args[0], "-01")
		return s
	} else if len(args) == 2 {
		// args[0] + "-" + args[1] + "-01"
		s, _ := hustrings.BuilderWriteStr(args[0], "-", args[1], "-01")
		return s
	}
	// time.Now().Format("2006-01") + "-01"
	s, _ := hustrings.BuilderWriteStr(time.Now().Format("2006-01"), "-01")
	return s
}

// YearFirtsDay 2021年9月23日
// huhutime.MonthFirstDay() ==> 2021-01-01
// huhutime.MonthFirstDay("1999") ==> 1999-01-01
func YearFirtsDay(yearstr ...string) string {
	if len(yearstr) == 1 {
		// yearstr[0] + "-01-01"
		s, _ := hustrings.BuilderWriteStr(yearstr[0], "-01-01")
		return s
	}
	// time.Now().Format("2006") + "-01-01"
	s, _ := hustrings.BuilderWriteStr(time.Now().Format("2006"), "-01-01")
	return s
}
