package humodbus

import (
	"encoding/hex"
	"fmt"
	"log"
	"testing"
)

func Test_demo(t *testing.T) {
	StrToFloat32("3FC00000")
	getv := []byte{0x00, 0x00, 0x3f, 0xc0}
	getv = GetDCBA(getv, BADC)
	fmt.Printf("%x\n", getv)
	f := ByteToFloat32(getv)
	fmt.Println(f)
	h1, _ := hex.DecodeString("3FC00000")
	fmt.Printf("%T, %x", h1, h1)
}

func Test2(t *testing.T) {
	fmt.Println(Bytes2Data([]byte{0xff, 0xff, 0xff, 0xff}, DataTypeLong))
	fmt.Println(ByteToInt16([]byte{0xff, 0xfb}))
	fmt.Println(ByteToLong([]byte{0xff, 0xfa, 0x10, 0x09}))
	fmt.Println(Data2bytes("-1", DataTypeLong))
	fmt.Println(Data2bytes("2", DataTypeUlong))
	fmt.Println(Data2bytes("123.45", DataTypeFloat))
}

func Test3(t *testing.T) {
	bs := []byte{0x01}
	log.Printf("%02x \n", CRC16(bs))
	log.Printf("%02x \n", ModbusCRC(bs))
	resp := []byte{0x01, 0x03, 0x06, 0x01, 0x15, 0x01, 0x03, 0x02, 0x03, 0x9d, 0xfa}
	log.Println(ModbusCRCCheck(resp))
}
