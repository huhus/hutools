package humodbus

// 添加MBAP头
func AddMBAPHead(cmdbody []byte, args ...byte) []byte {
	cmd := []byte{0x01, 0x00, 0x00, 0x00, 0x00, 0x06}
	// log.Println(args)
	for i := 0; i < len(args); i++ {
		cmd[i] = args[i]
	}
	cmd[5] = byte(len(cmdbody))
	return cmd
}
