package humodbus

import (
	"sync"
)

// Cyclical Redundancy Checking.
var (
	once     sync.Once
	crtTable []uint16
)

// CRC16直接法
func ModbusCRC(bs []byte) uint16 {
	crcPoly16 := uint16(0xa001)
	crc := uint16(0xFFFF)  // (1)．预置1个16位的寄存器为十六进制FFFF（即全为1）；称此寄存器为CRC寄存器；
	for _, b := range bs { // 6)．重复步骤2到步骤5，进行通讯信息帧下一个字节的处理；
		crc ^= uint16(b)         // (2)．把第一个8位二进制数据（既通讯信息帧的第一个字节）与16位的CRC寄存器的低8位相异或，把结果放于CRC寄存器；
		for i := 0; i < 8; i++ { // (5)．重复步骤3和4，直到右移8次，这样整个8位数据全部进行了处理；
			if (crc)&0x0001 == 1 {
				crc >>= 1        // (3)．把CRC寄存器的内容右移一位（朝低位）用0填补最高位，并检查右移后的移出位；
				crc ^= crcPoly16 // (4).如果移出位为1：CRC寄存器与多项式A001（1010 0000 0000 0001）进行异或；
			} else { // (4)．如果移出位为0：重复第3步（再次右移一位）；
				crc >>= 1 // (3)．把CRC寄存器的内容右移一位（朝低位）用0填补最高位，并检查右移后的移出位；
			}

		}
	}

	// crc = crc<<8 | crc>>8 // (7)．将该通讯信息帧所有字节按上述步骤计算完成后，得到的16位CRC寄存器的高、低字节进行交换；
	return crc
}

// CRC16 Calculate Cyclical Redundancy Checking.
func CRC16(bs []byte) uint16 {
	once.Do(initCrcTable)
	val := uint16(0xFFFF)
	for _, v := range bs {
		val = (val >> 8) ^ crtTable[(val^uint16(v))&0x00FF]
	}
	return val
}

// CRC16Byte 得到CRC16 byte[]
func CRC16Byte(bs []byte) []byte {
	once.Do(initCrcTable)

	val := uint16(0xFFFF)
	for _, v := range bs {
		val = (val >> 8) ^ crtTable[(val^uint16(v))&0x00FF]
	}
	return []byte{byte(val >> 8), byte(val)}
}

// CRC校验
func ModbusCRCCheck(resp []byte) bool {
	crc := CRC16(resp[:len(resp)-2])
	return crc == uint16(resp[len(resp)-2])|uint16(resp[len(resp)-1])<<8
}

// initCrcTable 初始化表.
func initCrcTable() {
	crcPoly16 := uint16(0xa001)
	crtTable = make([]uint16, 256)

	for i := uint16(0); i < 256; i++ {
		crc := uint16(0)
		b := i

		for j := uint16(0); j < 8; j++ {
			if ((crc ^ b) & 0x0001) > 0 {
				crc = (crc >> 1) ^ crcPoly16
			} else {
				crc >>= 1
			}
			b >>= 1
		}
		crtTable[i] = crc
	}
}
