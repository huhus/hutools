package humodbus

import (
	"encoding/binary"
	"fmt"
	"math"
	"strconv"
)

/*
	数据类型 [0, 10) 少于16位, [10, 20) 16位, [20, 30) 32位, [30, 40) 64位.
	通过 / 10 得到 商 [0,1,2,3] >> [<16, 16, 32, 46]
*/
const (
	DataTypeBool = 0 // 开关类型

	DataTypeInt  = 10 // 16整型
	DataTypeUint = 11 // 16无符号整型

	DataTypeLong  = 20 // 32长整型
	DataTypeUlong = 21 // 32无符号整型
	DataTypeFloat = 22 // 32浮点型

	DataTypeDouble = 30 // 64浮点型
)

const (
	ABCD = 0
	DCBA = 1
	CDAB = 2
	BADC = 3
)

// 字节数组转数值
func Bytes2Data(databyte []byte, datatype int) interface{} {
	data := interface{}(nil)
	switch datatype {
	case DataTypeInt:
		data = ByteToInt16(databyte)
	case DataTypeUint:
		data = ByteToUint16(databyte)
	case DataTypeLong:
		data = ByteToLong(databyte)
	case DataTypeUlong:
		data = ByteToUint32(databyte)
	case DataTypeFloat:
		data = ByteToFloat32(databyte)
	case DataTypeDouble:
		data = ByteToFloat64(databyte)
	}
	return data
}

// 数值转字节数组
func Data2bytes(datastr string, datatype int) (databytes []byte) {
	switch datatype {
	case DataTypeInt:
		val, _ := strconv.ParseInt(datastr, 10, 16)
		databytes = make([]byte, 2)
		binary.BigEndian.PutUint16(databytes, uint16(val))
	case DataTypeUint:
		val, _ := strconv.ParseUint(datastr, 10, 16)
		databytes = make([]byte, 2)
		binary.BigEndian.PutUint16(databytes, uint16(val))
	case DataTypeLong:
		val, _ := strconv.ParseInt(datastr, 10, 32)
		databytes = make([]byte, 4)
		binary.BigEndian.PutUint32(databytes, uint32(val))
	case DataTypeUlong:
		val, _ := strconv.ParseUint(datastr, 10, 32)
		databytes = make([]byte, 4)
		binary.BigEndian.PutUint32(databytes, uint32(val))
	case DataTypeFloat:
		val, _ := strconv.ParseFloat(datastr, 32)
		databytes = make([]byte, 4)
		// binary.BigEndian. (databytes, uint32(val))
		u32 := math.Float32bits(float32(val))
		binary.BigEndian.PutUint32(databytes, u32)
	case DataTypeDouble:

	}
	return databytes
}

// 大端模式用DCBA
// 测试用Modbus slave ABCD的数据要用DCBA才能正确获取
func GetDCBA(in []byte, types int) (out []byte) {
	if len(in) == 4 {
		switch types {
		case ABCD:
			out = []byte{in[3], in[2], in[1], in[0]}
		case DCBA:
			out = in
		case CDAB:
			out = []byte{in[1], in[0], in[3], in[2]}
		case BADC:
			out = []byte{in[2], in[3], in[0], in[1]}
		}
	} else if len(in) == 2 {
		switch types {
		case ABCD:
			fallthrough
		case CDAB:
			out = []byte{in[1], in[0]}
		default:
			out = in
		}
	}

	return
}

// uint162Bytes creates a sequence of uint16 data.
func Uint162Bytes(value ...uint16) []byte {
	data := make([]byte, 2*len(value))
	for i, v := range value {
		binary.BigEndian.PutUint16(data[i*2:], v)
	}
	return data
}

// bytes2Uint16 bytes convert to uint16 for register.
func Bytes2Uint16(buf []byte) []uint16 {
	data := make([]uint16, 0, len(buf)/2)
	for i := 0; i < len(buf)/2; i++ {
		data = append(data, binary.BigEndian.Uint16(buf[i*2:]))
	}
	return data
}

// StrToFloat32 hexstr convent to float32
func StrToFloat32(i string) float32 {
	// i := "3E93C1CD"
	// 字符串 按16进制，32bit转换成 uint64
	n, _ := strconv.ParseUint(i, 16, 32)
	fmt.Printf("%T, %v\r\n", n, n)
	f := math.Float32frombits(uint32(n))
	fmt.Printf("%T, %v\r\n", f, f)
	return f
}

// Float32ToStr float32 convent to hexstr
func Float32ToStr(f float32) string {
	n := math.Float32bits(f)
	// fmt.Printf("%T, %v\r\n", n, n)
	var hexstr = ""
	// 1、直接格式16进制字符串
	hexstr = fmt.Sprintf("%x", n)
	// fmt.Printf("%T, %v\r\n", hexstr, hexstr)
	// 2、使用strconv.FormatUint 把uint64转换成16进制字符串
	// hexstr = strconv.FormatUint(uint64(n), 16)
	// fmt.Printf("%T, %v\r\n", hexstr, hexstr)
	return hexstr
}

func Float32ToByte(float float32) []byte {
	bits := math.Float32bits(float)
	bytes := make([]byte, 4)
	// binary.LittleEndian.PutUint32(bytes, bits)
	binary.BigEndian.PutUint32(bytes, bits)

	return bytes
}

// func ByteToInt(bytes []byte) int16 {
// 	var num int16
// 	for _, b := range bytes {
// 		num = num << 8
// 		num = num | int16(b)
// 	}
// 	return num
// }

// 大小端模式需要验证
func ByteToInt16(bytes []byte) int {
	// bytes = GetDCBA(bytes, DCBA)
	var num int16
	for _, b := range bytes {
		num = num << 8
		num = num | int16(b)
	}
	return int(num)
}

// 大小端模式需要验证
func ByteToLong(bytes []byte) int {
	var num int32
	// bytes = GetDCBA(bytes, DCBA)
	for _, b := range bytes {
		num = num << 8
		num = num | int32(b)
	}
	return int(num)
}

func ByteToUint16(bytes []byte) uint16 {
	// bits := binary.LittleEndian.Uint32(bytes)
	return binary.BigEndian.Uint16(bytes)
}

func ByteToUint32(bytes []byte) uint32 {
	// bits := binary.LittleEndian.Uint32(bytes)
	return binary.BigEndian.Uint32(bytes)
}

func ByteToFloat32(bytes []byte) float32 {
	// bits := binary.LittleEndian.Uint32(bytes)
	bits := binary.BigEndian.Uint32(bytes)

	return math.Float32frombits(bits)
}

func Float64ToByte(float float64) []byte {
	bits := math.Float64bits(float)
	bytes := make([]byte, 8)
	binary.BigEndian.PutUint64(bytes, bits)

	return bytes
}

func ByteToFloat64(bytes []byte) float64 {
	bits := binary.BigEndian.Uint64(bytes)
	return math.Float64frombits(bits)
}
