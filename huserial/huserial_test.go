package huserial

import (
	"log"
	"strconv"
	"testing"
	"time"

	"github.com/tarm/serial"
)

func Test_demo(t *testing.T) {
	c := &serial.Config{Name: "COM15", Baud: 115200}
	c.ReadTimeout = time.Second
	c.Parity = serial.ParityNone
	c.StopBits = serial.Stop1
	c.Size = 8
	s, err := serial.OpenPort(c)
	if err != nil {
		log.Fatal(err)
	}

	n, err := s.Write([]byte("test"))
	if err != nil {
		log.Fatal(n, err)
	}

	buf := make([]byte, 128)
	n, err = s.Read(buf)
	if err != nil {
		log.Fatal("err", err)
	}
	if n == 0 {
		log.Println("time out")
	} else {
		log.Printf("%q", buf[:n])
	}

}

func Test2(t *testing.T) {
	s := NewSerial("com1", 115200)
	log.Println(s.Open())
	go func() {
		i := 0
		for {
			s.Write([]byte("abcd" + strconv.Itoa(i)))
			i++
			time.Sleep(time.Millisecond * 100)
		}

	}()

	for {
		buff := make([]byte, 100)
		s.Read(buff)
		time.Sleep(time.Millisecond * 100)
		log.Println(string(buff))
	}

	s.Close()
}
