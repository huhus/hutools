package huserial

import (
	"time"

	"github.com/tarm/serial"
)

type HuSerial struct {
	Config *serial.Config
	Port   *serial.Port
}

func NewSerial(name string, baud int) *HuSerial {
	s := &HuSerial{Config: &serial.Config{}}
	// 加上这个就可以读com10以上的串口了
	if len(name) > 0 && name[0] != '\\' {
		name = "\\\\.\\" + name
	}
	s.Config.Name = name
	s.Config.Baud = baud
	s.Config.ReadTimeout = time.Second
	s.Config.Parity = serial.ParityNone
	s.Config.StopBits = serial.Stop1
	s.Config.Size = 8
	return s
}

// 打开串口
func (s *HuSerial) Open() error {
	p, err := serial.OpenPort(s.Config)
	s.Port = p
	return err
}

// 关闭串口
func (s *HuSerial) Close() error {
	return s.Port.Close()
}

// 读数据
func (s *HuSerial) Write(buff []byte) (int, error) {
	return s.Port.Write(buff)
}

// 写数据
func (s *HuSerial) Read(buff []byte) (int, error) {
	return s.Port.Read(buff)
}
