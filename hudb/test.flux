from(bucket:"foxconnA")|> range(start: -2h) |> filter(fn: (r) => r._measurement == "odata")

from(bucket: "foxconnA")
  |> range(start: -1m)
  |> filter(fn: (r) => r["_measurement"] == "odatamp")
  |> group(columns: ["_time"])