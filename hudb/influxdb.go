package hudb

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

const host = "http://8.141.56.118:8086"
const Token = "V4M9zTYmrRWrMnzAtVlAYWdf8h5H8AhbgBg9ud0JV5b_FhF7_A-UKJvxwUbN6eaiM_10t6XkXcvKN4IzSIdTWw=="
const localhost = "http://127.0.0.1:8086"
const localToken = "kB77-yxOOz0UlA-NzQuF8AesC5X8XbQVfn_1udv7ngoceB3fNJjLU98Wggjd0YJAhbS5AmwdyFrQ1C8CXIeygg=="

var runhost, runToken string

func init() {
	runhost = host
	runToken = Token
}

func SetLocal() {
	runhost = localhost
	runToken = localToken
}

func Write(avg, max float64) {
	// Create a client
	// You can generate an API Token from the "API Tokens Tab" in the UI
	client := influxdb2.NewClient(runhost, runToken)
	// always close client at the end
	defer client.Close()

	writeAPI := client.WriteAPI("foxconn", "foxconnA")
	// write line protocol
	// writeAPI.WriteRecord(fmt.Sprintf("stat,unit=temperature avg=%f,max=%f,avg2=%f", avg, max, max))
	writeAPI.WriteRecord(fmt.Sprintf("stat,unit=temprature,unit2=aaa avg=%f,max=%f,avg2=%f", avg, max, max))
	// Flush writes
	writeAPI.Flush()
}

func WriteWithPoint(avg, max float64) {
	// Create a client
	// You can generate an API Token from the "API Tokens Tab" in the UI
	client := influxdb2.NewClient(runhost, runToken)
	// always close client at the end
	defer client.Close()

	writeAPI := client.WriteAPI("foxconn", "foxconnA")
	// write line protocol
	// writeAPI.WriteRecord(fmt.Sprintf("stat,unit=temperature avg=%f,max=%f,avg2=%f", avg, max, max))
	// writeAPI.WriteRecord(fmt.Sprintf("stat,unit=temprature,unit2=aaa avg=%f,max=%f,avg2=%f", avg, max, max))
	// p := influxdb2.NewPointWithMeasurement("stat").AddTag("unit", "bbb").AddTag("uint2", "ccc")
	p := influxdb2.NewPointWithMeasurement("stat").AddTag("unit", "bbb")
	p.AddField("avg", avg)
	writeAPI.WritePoint(p)
	p.AddField("avg2", avg).AddField("max", max)
	writeAPI.WritePoint(p)
	// Flush writes
	time.Sleep(time.Second * 10)
	writeAPI.Flush()
}

func Read() {
	// Create a client
	// You can generate an API Token from the "API Tokens Tab" in the UI
	client := influxdb2.NewClient(runhost, runToken)
	// always close client at the end
	defer client.Close()

	// Get query client
	queryAPI := client.QueryAPI("foxconn")

	query := `from(bucket:"foxconnA")|> range(start: -2h) |> filter(fn: (r) => r._measurement == "stat")`

	// get QueryTableResult
	result, err := queryAPI.Query(context.Background(), query)
	if err != nil {
		panic(err)
	}

	// Iterate over query response
	for result.Next() {
		// Notice when group key has changed
		// if result.TableChanged() {
		// 	fmt.Printf("table: %s\n", result.TableMetadata().String())
		// }
		// fmt.Println(result.Record())
		// Access data
		fmt.Printf("field:%v, value: %v, time:%v\n", result.Record().Field(), result.Record().Value(), result.Record().Time())
	}
	// check for an error
	if result.Err() != nil {
		fmt.Printf("query parsing error: %s\n", result.Err().Error())
	}
}

func APIQuery() {
	client := &http.Client{}

	req, err := http.NewRequest("POST", runhost+"/api/v2/query?org=foxconn", strings.NewReader(`from(bucket:"foxconnA")|> range(start: -2h) |> filter(fn: (r) => r._measurement == "stat")`))
	if err != nil {
		// handle error
		fmt.Println(err)
	}

	req.Header.Set("Authorization", "Token "+runToken)
	// req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept-Type", "application/csv")
	req.Header.Set("Content-Type", "application/vnd.flux")
	req.Header.Set("Accept-Encoding", "gzip")
	resp, err := client.Do(req)
	if err != nil {
		defer resp.Body.Close()
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(body))
	resstr := string(body)
	rows := strings.Split(resstr, "\n")
	for _, v := range rows {
		fmt.Println(strings.Split(v, ","))
	}
}
