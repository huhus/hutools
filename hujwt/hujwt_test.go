package hujwt

import (
	"fmt"
	"testing"
	"time"
)

func Test_demo(t *testing.T) {
	token, _ := GenerateToken("123876", "3", 3)
	fmt.Println("生成的token:", token)
	claim, err := ParseToken(token)
	if err != nil {
		fmt.Println("解析token出现错误：", err)
	} else if time.Now().Unix() > claim.ExpiresAt {
		fmt.Println("时间超时")
	} else {
		fmt.Println("username:", claim.ID)
		fmt.Println("password:", claim.Power)
	}
}
