package hujwt

import (
	jwt "github.com/dgrijalva/jwt-go"

	"time"
)

var jwtSecret = []byte("hujwt")

type Claims struct {
	ID    string `json:"id"`
	Power string `json:"power"`
	jwt.StandardClaims
}

func SetjwtSecret(mySecret string) {
	jwtSecret = []byte(mySecret)
}

// 产生token的函数
func GenerateToken(id, power string, hours time.Duration) (string, error) {
	nowTime := time.Now()
	if hours == 0 {
		hours = 2
	}
	expireTime := nowTime.Add(hours * time.Hour)

	claims := Claims{
		id,
		power,
		jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			Issuer:    "gin-blog",
		},
	}
	//
	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := tokenClaims.SignedString(jwtSecret)

	return token, err
}

// 验证token的函数
func ParseToken(token string) (*Claims, error) {
	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})

	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}
	//
	return nil, err
}
