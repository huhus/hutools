package huhex

import (
	"encoding/hex"
	"strings"
)

func Bytes2Hexstr(b []byte) string {
	return hex.EncodeToString(b)
}

func HexStr2Bytes(hexstr string) ([]byte, error) {
	hexstr = strings.ReplaceAll(hexstr, " ", "")
	return hex.DecodeString(hexstr)
}

func CheckSum(readCMD string) string {
	rcmdb, _ := HexStr2Bytes(readCMD)
	var cs byte
	for _, by := range rcmdb {
		cs += by
	}
	return Bytes2Hexstr([]byte{cs})
}
