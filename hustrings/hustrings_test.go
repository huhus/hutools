package hustrings

import (
	"fmt"
	"regexp"
	"testing"
)

// ^开头,$结尾, \d数字, \w字母或数字。
// ^(F3Y) 所有以F3Y开头的 F3Y0X3W2190VAF，F3Y0X3W2190VAN，F3Y0X3W2190VAN222222
// ^(F3Y)(\w{11})$ 所有以F3Y开头且后边接11位字母或数字，F3Y0X3W2190VAF，F3Y0X3W2190VAN
// ^(F3Y)(\w{8})VAN$ 所有以F3Y开头且后边接8位字母或数字,再以VAN结束，F3Y0X3W2190VAN

func Test_demo2(t *testing.T) {
	// ddd()
	// aa()
	// sp()
	// pd()
	// pindf("DICHAN", "F")
	fmt.Println(difarry("INPUTCHAN%d_PIN", 36))
}

func TestRE(t *testing.T) {
	buf := "abc azc a7c aac 888 a9c  tac"

	//解析正则表达式，如果成功返回解释器
	reg1 := regexp.MustCompile(`a.c`)
	if reg1 == nil {
		fmt.Println("regexp err")
		return
	}
	//根据规则提取关键信息
	result1 := reg1.FindAllStringSubmatch(buf, -1)
	// result1 = [[abc] [azc] [a7c] [aac] [a9c]]
	fmt.Println("result1 =", result1)

	usere := 2

	buf2 := "F3Y0X3W2190VAF"
	buf3 := "AF3Y0X3W2190VAF"
	// ^(F3Y)(\w{11})
	reg2 := regexp.MustCompile(`^(F3Y)`)
	if reg2 == nil {
		fmt.Println("regexp err")
		return
	}
	// result2 := reg2.FindString(buf2)
	// result3 := reg2.FindString(buf3)

	if usere == 1 {
		if reg2.MatchString(buf2) {
			fmt.Println("result2 =", buf2)
		}
		// if result2 != "" {
		// 	fmt.Println("result2 =", result2)
		// }
	} else if usere == 2 {
		if !reg2.MatchString(buf3) {
			fmt.Println("result2 =", buf3)
		}
	}

}
