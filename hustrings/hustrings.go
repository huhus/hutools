package hustrings

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func aa() {
	for i := 1; i < 11; i++ {
		fmt.Println(fmt.Sprintf(`cmd = strings.ReplaceAll(cmd, "[E_Zone%d_Fan_Speed_Up]", ld.UF%d)`, i, i))
		fmt.Println(fmt.Sprintf(`cmd = strings.ReplaceAll(cmd, "[E_Zone%d_Fan_Speed_Down]", ld.DF%d)`, i, i))
	}
}

func pd() {
	port := "E"
	v := `#define INPUTCHAN%d_PIN                         GPIO_PIN_%d
#define INPUTCHAN%d_GPIO_PORT                   GPIO%s
#define INPUTCHAN%d_GPIO_CLK                    RCU_GPIO%s`
	for i := 33; i < 37; i++ {
		fmt.Printf(v+"\n", i, i-33, i, port, i, port)
	}
}

func pindf(name, port string) {
	v := `#define [name]%d_PIN                         GPIO_PIN_%d
#define [name]%d_GPIO_PORT                   GPIO%s
#define [name]%d_GPIO_CLK                    RCU_GPIO%s`
	v = strings.ReplaceAll(v, `[name]`, name)
	for i := 4; i < 9; i++ {
		fmt.Printf(v+"\n", i, i-4, i, port, i, port)
	}
}

func difarry(name string, n int) string {
	str := "{"
	for i := 1; i < n+1; i++ {
		str = str + fmt.Sprintf(name, i)
		if i != n {
			str = str + ", "
		}
	}
	str = str + "}"
	return str
}
func sp() {
	v := `{upload: true,name: "%s",setval: %d,usl: %d,lsl: %d,check: true,alarm: true,smema: true,unit: "%s",},`
	file, err := os.Create("result.txt")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()
	file.WriteString(fmt.Sprintf(v, "Carrier2Carrier_Gap", 10, -1, -1, "s"))
	for i := 0; i < 10; i++ {
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("Probe%d_Peak_Temp", i+1), 228, 3, 2, "degree"))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("Probe%d_Rising_Slope", i+1), 1, 1, 1, ""))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("Probe%d_TL_Liquidous_Temp", i+1), 60, 5, 5, "s"))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("Probe%d_Cooling_Down_Slope", i+1), -3, 1, 1, ""))
		// HTS
		// file.WriteString(fmt.Sprintf(v, fmt.Sprintf("Probe%d_TS_Soak_Temp", i+1), 100, 10, 10, "s"))
		// file.WriteString(fmt.Sprintf(v, fmt.Sprintf("Probe%d_TR_Ramp_Temp", i+1), 35, 5, 5, "s"))
	}
	file.WriteString(fmt.Sprintf(v, "Profile_Result", 1, -1, -1, ""))
}

func ddd() {
	v := `{upload: true,name: "%s",setval: %d,usl: %d,lsl: %d,check: true,alarm: true,smema: true,unit: "%s",},`
	file, err := os.Create("result.txt")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()
	for i := 0; i < 10; i++ {
		set := 1620
		usl := 162
		lsl := 162
		unit := "rpm"
		// fmt.Println(fmt.Sprintf(v, fmt.Sprintf("E_Zone%d_Fan_Speed_Up", i+1), set, usl, lsl, unit))
		// fmt.Println(fmt.Sprintf(v, fmt.Sprintf("E_Zone%d_Fan_Speed_down", i+1), set, usl, lsl, unit))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("E_Zone%d_Fan_Speed_Up", i+1), set, usl, lsl, unit))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("E_Zone%d_Fan_Speed_down", i+1), set, usl, lsl, unit))
	}
	file.WriteString(fmt.Sprintf(v, "Chain_Speed", 790, 50, 50, "mm/min"))
	for i := 0; i < 10; i++ {
		set := 155
		usl := 3
		lsl := 3
		unit := "degree"
		// fmt.Println(fmt.Sprintf(v, fmt.Sprintf("B_Zone%d_Sensor_Up", i+1), set, usl, lsl, unit))
		// fmt.Println(fmt.Sprintf(v, fmt.Sprintf("B_Zone%d_Sensor_Down", i+1), set, usl, lsl, unit))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("B_Zone%d_Sensor_Up", i+1), set, usl, lsl, unit))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("B_Zone%d_Sensor_Down", i+1), set, usl, lsl, unit))
	}
	for i := 0; i < 10; i++ {
		set := 155
		usl := 3
		lsl := 3
		unit := "degree"
		// fmt.Println(fmt.Sprintf(v, fmt.Sprintf("B_Zone%d_Sensor_Up", i+1), set, usl, lsl, unit))
		// fmt.Println(fmt.Sprintf(v, fmt.Sprintf("B_Zone%d_Sensor_Down", i+1), set, usl, lsl, unit))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("E_Zone%d_Sensor_Left", i+1), set, usl, lsl, unit))
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("E_Zone%d_Sensor_Right", i+1), set, usl, lsl, unit))
	}
	for i := 0; i < 10; i++ {
		set := 500
		usl := 500
		lsl := 470
		unit := "DPPM"
		file.WriteString(fmt.Sprintf(v, fmt.Sprintf("E_Zone%d_Oxygen_Content", i+1), set, usl, lsl, unit))
	}
	file.WriteString(fmt.Sprintf(v, "Reflow_time", -1, -1, -1, "s"))
	file.WriteString(fmt.Sprintf(v, "Health_Index", -1, -1, -1, ""))
}

func BuilderWriteStr(strs ...string) (string, error) {
	var b strings.Builder
	for _, str := range strs {
		_, err := b.WriteString(str)
		if err != nil {
			return b.String(), err
		}
	}
	return b.String(), nil
}

func ClearSpaces(is string) string {
	return strings.ReplaceAll(is, " ", "")
}
