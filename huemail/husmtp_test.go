package huemail

import (
	"fmt"
	"testing"
)

func TestSendEmail(t *testing.T) {
	user := "lgh950122@163.com"
	password := "PYJTXJYHYTWNOKFZ"
	host := "smtp.163.com:25"
	to := "924003963@qq.com;lgh950122@163.com"
	subject := "使用Golang发送邮件"

	body := `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="iso-8859-15">
			<title>MMOGA POWER</title>
		</head>
		<body>
			GO 发送邮件，官方连包都帮我们写好了，真是贴心啊！！！
		</body>
		</html>`

	sendUserName := "GOLANG SEND MAIL" //发送邮件的人名称
	fmt.Println("send email")
	err := SendToMail(user, sendUserName, password, host, to, subject, body, "html")
	if err != nil {
		fmt.Println("Send mail error!")
		fmt.Println(err)
	} else {
		fmt.Println("Send mail success!")
	}
}

func TestSend2(t *testing.T) {
	mail := &SendMail{user: "lgh950122@163.com", password: "PYJTXJYHYTWNOKFZ", host: "smtp.163.com", port: "25"}
	message := Message{from: "lgh950122@163.com", sendUserName: "huhu3",
		to: []string{"lgh950122@163.com", "924003963@qq.com"}, // 924003963@qq.com
		// 抄送
		cc:  []string{"924003963@qq.com"},
		bcc: []string{},
		// 标题
		subject: "测试",
		body: `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="iso-8859-15">
			<title>MMOGA POWER</title>
		</head>
		<body>
			GO 发送邮件，官方连包都帮我们写好了，真是贴心啊！！！
		</body>
		</html>`, // body:        "测试body"
		contentType: "text/html; charset=UTF-8", // 	contentType: "text/plain;charset=utf-8"
	}
	fmt.Println("准备发送邮件")
	mail.Send(message)
	fmt.Println("发送邮件成功")
}

func TestSendWithFile(t *testing.T) {
	mail := &SendMail{user: "lgh950122@163.com", password: "PYJTXJYHYTWNOKFZ", host: "smtp.163.com", port: "25"}
	message := Message{from: "lgh950122@163.com", sendUserName: "huhu2",
		to: []string{"lgh950122@163.com", "924003963@qq.com"}, // 924003963@qq.com
		// 抄送
		cc:  []string{"924003963@qq.com"},
		bcc: []string{},
		// 标题
		subject:     "测试",
		body:        "测试body",
		contentType: "text/plain;charset=utf-8",
		attachment: Attachment{
			name: "husmtp.go",
			// 使用默认类型
			contentType: "application/octet-stream",
			withFile:    true,
		},
	}
	fmt.Println("准备发送邮件")
	mail.Send(message)
	fmt.Println("发送邮件成功")
}
