package huencode

import (
	"bytes"
	"image/png"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code128"
	"github.com/skip2/go-qrcode"
)

// 二维码，条形码生成与解析。

// 生成二维码PNG bese64 bytes
// 前端使用 <img src="data:image/png;base64," + {string(PNG bytes)}/>
func QRCodeEncode(content string, level qrcode.RecoveryLevel, size int) (pngb []byte, err error) {
	pngb, err = qrcode.Encode(content, level, size)
	if err != nil {
		return
	}
	// 这个方法转base64头尾会加双引号
	// js, _ := json.Marshal(png)
	// log.Print("qrcodejson", string(js))

	// 转base64
	pngb = []byte(coder.EncodeToString(pngb))
	return
}

// 生成条码PNG bese64 bytes
// 前端使用 <img src="data:image/png;base64," + {string(PNG bytes)}/>
func BarCodeEncode(content string, width, height int) (pngb []byte, err error) {
	code, _ := code128.Encode(content)
	ncode, _ := barcode.Scale(code, width, height)
	bf := bytes.Buffer{}
	err = png.Encode(&bf, ncode)
	if err != nil {
		return
	}
	pngb = []byte(coder.EncodeToString(bf.Bytes()))
	return
}
