package huencode

import (
	"encoding/base64"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func Base64ImgDataToFile(filepath string, base64data string) {
	// The actual image starts after the ","
	i := strings.Index(base64data, ",")
	if i < 0 {
		log.Fatal("no comma")
	}
	// pass reader to NewDecoder
	dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(base64data[i+1:]))
	f, _ := os.Create(filepath)
	defer f.Close()

	// 一次全部读取就可以
	wb, _ := ioutil.ReadAll(dec)
	f.Write(wb)
}
