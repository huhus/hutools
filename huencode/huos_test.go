package huencode

import (
	"bytes"
	"fmt"
	"image/png"
	"log"
	"os"
	"testing"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code128"
)

func Test(t *testing.T) {
	// fmt.Println(len("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"))
	no := GetBaseBoardID()
	fmt.Println(no)
	// fmt.Printf("%02x \n", no)
	// daystr := "3000"
	encodestr := Base64Encode(no, "123", "2222")
	fmt.Println("len", len(encodestr))
	fmt.Println(encodestr)
	fmt.Println(Base64Decode(encodestr, "123", "2222"))
	encodestr = Base64Encode(encodestr)
	fmt.Println(encodestr)
	fmt.Println(Base64Decode(encodestr))
}

func Test2(t *testing.T) {
	pngb, err := QRCodeEncode("www.baidu.com", 1, 256)
	log.Print(err, pngb)
	// js, _ := json.Marshal(pngb)
	log.Print(string(pngb))
}

type mywriter struct {
	Buff []byte
}

func (w *mywriter) Write(p []byte) (n int, err error) {
	w.Buff = append(w.Buff, p...)
	return len(p), nil
}

func TestBarcode(t *testing.T) {
	txt := "A40A156B"
	code, _ := code128.Encode(txt)

	// Scale the barcode to 200x200 pixels
	qrCode, _ := barcode.Scale(code, code.Bounds().Max.X, 50)

	// create the output file
	file, _ := os.Create("qrcode.png")
	defer file.Close()
	w := mywriter{}
	w.Buff = []byte{}
	bf := bytes.Buffer{}

	// encode the barcode as png
	png.Encode(file, qrCode)
	png.Encode(&w, qrCode)
	png.Encode(&bf, qrCode)
	log.Print(coder.EncodeToString(w.Buff))
	log.Print(coder.EncodeToString(bf.Bytes()))

	log.Print(BarCodeEncode("AABBCCDD", 200, 30))
}

func TestSerial(t *testing.T) {
	log.Println(GetAllSerial())
}
