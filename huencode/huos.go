package huencode

import (
	"encoding/base64"
	"fmt"
	"os/exec"
	"runtime"
	"strings"
)

const (
	//BASE64字符表,不要有重复
	base64Table        = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	hashFunctionHeader = "zh.ife.iya"
	hashFunctionFooter = "09.O25.O20.78"
)

var coder = base64.NewEncoding(base64Table)

/**
 * base64加密
 */
func Base64Encode(str string, salt ...string) string {
	heander := hashFunctionHeader
	footer := hashFunctionFooter
	if len(salt) == 2 {
		heander = salt[0]
		footer = salt[1]
	}

	var src []byte = []byte(heander + str + footer)
	return string([]byte(coder.EncodeToString(src)))
}

/**
 * base64解密
 */
func Base64Decode(str string, salt ...string) (string, error) {
	heander := hashFunctionHeader
	footer := hashFunctionFooter
	if len(salt) == 2 {
		heander = salt[0]
		footer = salt[1]
	}
	var src []byte = []byte(str)
	by, err := coder.DecodeString(string(src))
	return strings.Replace(strings.Replace(string(by), heander, "", -1), footer, "", -1), err
}

//GetDiskDriveSN 获取硬盘SN
func GetDiskDriveSN() string {
	var diskDriveSN string
	system := runtime.GOOS
	switch system {
	case "windows":
		cmd := exec.Command("wmic", "DISKDRIVE", "get", "serialnumber")
		b, e := cmd.CombinedOutput()

		if e == nil {
			diskDriveSN = string(b)
			diskDriveSN = strings.ReplaceAll(diskDriveSN, "\n", "")
			diskDriveSN = strings.ReplaceAll(diskDriveSN, "\r", "")
			diskDriveSN = strings.ReplaceAll(diskDriveSN, "SerialNumber", "")
			diskDriveSN = strings.TrimSpace(diskDriveSN)
		} else {
			fmt.Printf("%v", e)
		}
	}
	return diskDriveSN
}

//GetBaseBoardID 获取主板的id
func GetBaseBoardID() string {
	var baseBoardID string
	system := runtime.GOOS
	switch system {
	case "windows":
		cmd := exec.Command("wmic", "baseboard", "get", "serialnumber")
		b, e := cmd.CombinedOutput()

		if e == nil {
			baseBoardID = string(b)
			baseBoardID = strings.ReplaceAll(baseBoardID, "\n", "")
			baseBoardID = strings.ReplaceAll(baseBoardID, "\r", "")
			baseBoardID = strings.ReplaceAll(baseBoardID, "SerialNumber", "")
			baseBoardID = strings.TrimSpace(baseBoardID)
		} else {
			fmt.Printf("%v", e)
		}
	}

	return baseBoardID
}

func GetAllSerial() (SerialList []string) {
	system := runtime.GOOS
	switch system {
	case "windows":
		// 读取注册表法
		cmd := exec.Command("reg", "query", `HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM`)
		b, e := cmd.CombinedOutput()
		// log.Println(e, string(b))
		if e == nil {
			list := strings.Split(string(b), "\n")
			for _, v := range list {
				names := strings.Split(v, "    ")
				if len(names) >= 3 && strings.Contains(names[3], "COM") {
					SerialList = append(SerialList, names[3])
				}
			}
		}
	}
	return
}
