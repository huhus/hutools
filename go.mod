module gitee.com/huhus/hutools

go 1.17

require (
	github.com/boombuler/barcode v1.0.1
	github.com/deepmap/oapi-codegen v1.8.2 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gogf/gf v1.16.9
	github.com/influxdata/influxdb-client-go/v2 v2.7.0
	github.com/influxdata/line-protocol v0.0.0-20200327222509-2487e7298839 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/net v0.0.0-20210520170846-37e1c6afe023 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/clbanning/mxj v1.8.5-0.20200714211355-ff02cfb8ea28 // indirect
	github.com/eclipse/paho.mqtt.golang v1.4.2 // indirect
	github.com/fatih/color v1.12.0 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	go.opentelemetry.io/otel v1.0.0 // indirect
	go.opentelemetry.io/otel/trace v1.0.0 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
